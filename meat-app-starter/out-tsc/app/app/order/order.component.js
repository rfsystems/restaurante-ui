var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { OrderService } from "app/order/order.service";
import { OrderItem } from "app/order/order.model";
import { Router } from "@angular/router";
import { FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/do';
var OrderComponent = (function () {
    function OrderComponent(orderService, router, formBuilder) {
        this.orderService = orderService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.emailPatter = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        this.numberPatter = /^[0-9]*$/;
        this.delivery = 8;
        this.paymentOptions = [
            { label: 'Dinheiro', value: 'MON' },
            { label: 'Cartão Débido', value: 'DEB' }
        ];
    }
    OrderComponent_1 = OrderComponent;
    OrderComponent.prototype.ngOnInit = function () {
        this.orderForm = this.formBuilder.group({
            name: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
            email: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPatter)]),
            emailConfirmation: this.formBuilder.control('', [Validators.required, Validators.pattern(this.emailPatter)]),
            number: this.formBuilder.control('', [Validators.required, Validators.pattern(this.numberPatter)]),
            address: this.formBuilder.control('', [Validators.required, Validators.minLength(5)]),
            optionalAddress: this.formBuilder.control(''),
            paymentOption: this.formBuilder.control('', [Validators.required])
            //associando um validaros aum grupo
        }, { validator: OrderComponent_1.equalsTo });
    };
    //aqui pegamos o campo do form que queremos testar pelo grupo.Pagamento
    //criamos a logica de comparacao e retornamos um obj composto de chave tipo string e valor
    //essa chave fica sssociada ao grupo q esta associado o validaros, nesse caso ao formulario.group
    OrderComponent.equalsTo = function (group) {
        var email = group.get('email');
        var emailConfirmation = group.get('emailConfirmation');
        if (!email || !emailConfirmation) {
            return undefined;
        }
        else if (email.value !== emailConfirmation.value) {
            return { emailsNotMatch: true };
        }
        return undefined;
    };
    OrderComponent.prototype.itemsValue = function () {
        return this.orderService.itemsValue();
    };
    OrderComponent.prototype.cartItems = function () {
        return this.orderService.cartItems();
    };
    OrderComponent.prototype.incrementaQtd = function (item) {
        return this.orderService.incrementaQtd(item);
    };
    OrderComponent.prototype.decrementaQtd = function (item) {
        return this.orderService.decrementaQtd(item);
    };
    OrderComponent.prototype.remove = function (item) {
        return this.orderService.remove(item);
    };
    //CONVERTENDO CARTITEMS EM ODERITEM COM MAP
    OrderComponent.prototype.verificaCompra = function (order) {
        var _this = this;
        order.itemsCompra = this.cartItems()
            .map(function (item) { return new OrderItem(item.quantity, item.menuItem.id); });
        this.orderService.checkOrder(order)
            .do(function (orderId) {
            _this.orderId = orderId;
        })
            .subscribe(function (orderId) {
            _this.router.navigate(['/order-sumary']);
            _this.orderService.clear();
        });
    };
    OrderComponent.prototype.isOrderCompleted = function () {
        return this.orderId !== undefined;
    };
    OrderComponent = OrderComponent_1 = __decorate([
        Component({
            selector: 'mt-order',
            templateUrl: './order.component.html',
            styleUrls: ['./order.component.css']
        }),
        __metadata("design:paramtypes", [OrderService, Router, FormBuilder])
    ], OrderComponent);
    return OrderComponent;
    var OrderComponent_1;
}());
export { OrderComponent };
//# sourceMappingURL=order.component.js.map