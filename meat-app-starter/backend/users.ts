export class User{
  constructor(public email:string, public name:string, private password:string){}

  verificaEmailEPass(another:User):boolean{
    return another !== undefined && another.email === this.email && another.password === this.password
  }
}
export const users:{[key:string]:User} ={
  "admin@admin" : new User('admin@admin', 'admin','admin'),
  "admin2@admin" : new User('admin2@admin', 'admin2','admin2'),
}
