import { Component, OnInit } from '@angular/core';
import { ShopCartService } from "app/restaurant-detail/shop-cart/shop-cart.service";
import {trigger, state, style, transition, animate, keyframes} from '@angular/animations'

@Component({
  selector: 'mt-shop-cart',
  templateUrl: './shop-cart.component.html',
  styleUrls: ['./shop-cart.component.css'],
  animations:[
    trigger('row',[
      state('ready', style({opacity:1 })),
      transition('void => ready', animate('300ms 0s ease-in', keyframes([
        style({opacity:0, transform:'translateX(-30px)',offset:0}),
        style({opacity:0.8, transform:'translateX(-10px)',offset:0.8}),
        style({opacity:1, transform:'translateX(0px)',offset:1})
      ]))),
      transition('ready => void', animate('300ms 0s ease-out', keyframes([
        style({opacity:1, transform:'translateX(0px)',offset:0}),
        style({opacity:0.8, transform:'translateX(-10px)',offset:0.2}),
        style({opacity:0, transform:'translateX(30px)',offset:1})
      ])))
      ])
  ]
})
export class ShopCartComponent implements OnInit {

  constructor(private shopCartServ : ShopCartService) { }

  rowState = 'ready'

  ngOnInit() {
  }

  items(): any[]{
    return this.shopCartServ.items;
  }
  total() : number {
    return this.shopCartServ.total();
  }
  clear(){
    this.shopCartServ.clean();
  }
  removeItem(item:any){
    this.shopCartServ.removeItem(item)
  }
  addItem(item:any){
    this.shopCartServ.addItem(item)
  }

}
