import {HttpErrorResponse} from '@angular/common/http'
import {Observable} from 'rxjs/Observable'
import { ErrorHandler, Injectable, Injector, NgZone } from "@angular/core";
import { NotificationService } from "app/shared/messages/notification.service";
import { LoginService } from "app/security/login/login.service";

@Injectable()
export class AppErrorHandler extends ErrorHandler{

  constructor(private ns:NotificationService, private injector: Injector, private zone: NgZone){
    super()
  }

  handleError(error: HttpErrorResponse | any){
    if (error instanceof HttpErrorResponse) {
      const message = error.error.message
      this.zone.run(()=>{
        switch(error.status){
          case 401:
          this.injector.get(LoginService).handleLogin()
          break;
          case 403:
          this.ns.emitirMenssagem(message || 'Não autorizado!')
          break;
          case 404:
          this.ns.emitirMenssagem(message || 'Recurso não encontrado!')
          break;
        }
      })
    }
    super.handleError(error)
    // let errorMessage:string
    // if(error instanceof HttpErrorResponse){
    //   const body = error.error
    //   errorMessage = `Erro ${error.status} ao acessar a URL ${error.url} - ${error.statusText || ''} ${body}`
    // }else{
    //   errorMessage = error.toString();
    // }

  }

}
