import { Component, OnInit } from '@angular/core';
import { RadioOption } from "app/shared/radio/radio-option.model";
import { OrderService } from "app/order/order.service";
import { CartItem } from "app/restaurant-detail/shop-cart/cart-item.model";
import { Order, OrderItem } from "app/order/order.model";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
import 'rxjs/add/operator/do'

@Component({
  selector: 'mt-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orderForm:FormGroup
  orderId:string

emailPatter=/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
numberPatter=/^[0-9]*$/

  delivery:number = 8
  paymentOptions : RadioOption[] = [
    {label:'Dinheiro', value:'MON'},
    {label:'Cartão Débido', value:'DEB'}
  ]

  constructor(private orderService:OrderService, private router:Router, private formBuilder :FormBuilder) { }

  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      name: this.formBuilder.control('',[Validators.required, Validators.minLength(5)]),
      email:this.formBuilder.control('',[Validators.required, Validators.pattern(this.emailPatter)]),
      emailConfirmation:this.formBuilder.control('',[Validators.required, Validators.pattern(this.emailPatter)]),
      number:this.formBuilder.control('',[Validators.required, Validators.pattern(this.numberPatter)]),
      address:this.formBuilder.control('',[Validators.required, Validators.minLength(5)]),
      optionalAddress:this.formBuilder.control(''),
      paymentOption:this.formBuilder.control('',[Validators.required])
//associando um validaros aum grupo
    },{validator:OrderComponent.equalsTo})
  }
  //aqui pegamos o campo do form que queremos testar pelo grupo.Pagamento
  //criamos a logica de comparacao e retornamos um obj composto de chave tipo string e valor
  //essa chave fica sssociada ao grupo q esta associado o validaros, nesse caso ao formulario.group
  static equalsTo(group:AbstractControl):{[key:string]:boolean}{
    const email = group.get('email')
    const emailConfirmation = group.get('emailConfirmation')
    if (!email || !emailConfirmation) {
        return undefined
    }else if(email.value !== emailConfirmation.value){
      return {emailsNotMatch:true}
    }
    return undefined
  }
  itemsValue():number{
    return this.orderService.itemsValue()
  }

  cartItems():CartItem[]{
    return this.orderService.cartItems()
  }
  incrementaQtd(item:CartItem){
    return this.orderService.incrementaQtd(item)
  }
  decrementaQtd(item:CartItem){
    return this.orderService.decrementaQtd(item)
  }
  remove(item:CartItem){
    return this.orderService.remove(item)
  }
  //CONVERTENDO CARTITEMS EM ODERITEM COM MAP
  verificaCompra(order: Order){
    order.itemsCompra = this.cartItems()
      .map((item:CartItem)=>new OrderItem(item.quantity, item.menuItem.id))
    this.orderService.checkOrder(order)
      .do((orderId:string)=>{
        this.orderId = orderId
      })
      .subscribe((orderId:string)=>{
        this.router.navigate(['/order-sumary'])
        this.orderService.clear()
    });
  }

  isOrderCompleted():boolean{
    return this.orderId !== undefined
  }
}
