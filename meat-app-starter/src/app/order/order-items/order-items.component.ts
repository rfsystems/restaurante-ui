import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CartItem } from "app/restaurant-detail/shop-cart/cart-item.model";

@Component({
  selector: 'mt-order-items',
  templateUrl: './order-items.component.html'
})
export class OrderItemsComponent implements OnInit {

  constructor() { }

  @Input() items:CartItem[]
//eventos que serao disparados
  @Output() incrementaQtd = new EventEmitter<CartItem>()
  @Output() decrementaQtd = new EventEmitter<CartItem>()
  @Output() removeQtd = new EventEmitter<CartItem>()

  ngOnInit() {
  }
  //emitindo o evento passando o item
  emitirIncrementoQtd(item:CartItem){
      this.incrementaQtd.emit(item)
  }
  emitirDecrementoQtd(item:CartItem){
      this.decrementaQtd.emit(item)
  }
  emitirRemoveQtd(item:CartItem){
      this.removeQtd.emit(item)
  }


}
