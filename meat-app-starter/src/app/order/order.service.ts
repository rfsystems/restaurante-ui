import { Injectable } from "@angular/core";
import { ShopCartService } from "app/restaurant-detail/shop-cart/shop-cart.service";
import { CartItem } from "app/restaurant-detail/shop-cart/cart-item.model";
import { Order, OrderItem } from "app/order/order.model";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import 'rxjs/add/operator/map'
import {MEAT_API} from "../api";
// import { LoginService } from "app/security/login/login.service";
import { HttpHeaders } from "@angular/common/http";

@Injectable()
export class OrderService{

  constructor(private cartService:ShopCartService, private http:HttpClient){}

  cartItems():CartItem[]{
    return this.cartService.items;
  }
  incrementaQtd(item:CartItem){
    this.cartService.incrementaQtd(item)
  }
  decrementaQtd(item:CartItem){
    this.cartService.decrementaQtd(item)
  }
  remove(item:CartItem){
    this.cartService.removeItem(item)
  }
  itemsValue():number{
    return this.cartService.total()
  }
  clear(){
    this.cartService.clean();
  }

  checkOrder(order:Order): Observable<string>{
    // if (this.loginService.isLoggedIn()) {
      // console.log("token no order:"+this.loginService.user.acessToken)
    // }
    return this.http.post<Order>(`${MEAT_API}/orders`, order)
      .map(order=>order.id)
  }

}
