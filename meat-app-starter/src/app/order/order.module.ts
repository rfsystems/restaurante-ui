import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { OrderItemsComponent } from "app/order/order-items/order-items.component";
import { OrderComponent } from "app/order/order.component";
import { FreteComponent } from "app/order/frete/frete.component";
import { SharedModule } from "app/shared/shared.module";
import { LeaveOrderGuard } from "app/order/leave-order-guard";


const ROUTES : Routes=[
  //definindo a rota padrao do modulo
  {path:'', component:OrderComponent, canDeactivate:[LeaveOrderGuard]}
]

@NgModule({
  //declarando os componentes que farao parte desse modulo
  declarations:[OrderComponent, OrderItemsComponent, FreteComponent],
  //passando as rotas desse modulo
  imports:[RouterModule.forChild(ROUTES), SharedModule]
})
export class OrderModule{

}
