import { NgModule } from "@angular/core";
import { AboutComponent } from "app/about/about.component";
import { RouterModule, Routes } from "@angular/router";

const ROUTES : Routes=[
  //definindo a rota padrao do modulo
  {path:'', component:AboutComponent}
]

@NgModule({
  //declarando os componentes que farao parte desse modulo
  declarations:[AboutComponent],
  //passando as rotas desse modulo
  imports:[RouterModule.forChild(ROUTES)]
})
export class AboutModule{

}
