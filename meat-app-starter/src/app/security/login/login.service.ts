import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { MEAT_API } from "../../api";
import { User } from "app/security/login/user.model";
import "rxjs/add/operator/do";
import "rxjs/add/operator/filter";
import { Router, NavigationEnd } from "@angular/router";

@Injectable()
export class  LoginService {

  user:User
  lastUrl:string

  constructor(private http:HttpClient, private router:Router){
    this.router.events.filter(evento=> evento instanceof NavigationEnd)
      .subscribe((evento:NavigationEnd) => this.lastUrl = evento.url)
  }
  logout(){
    this.user = undefined
  }
  login(email:string, password:string):Observable<User>{
    return this.http.post<User>(`${MEAT_API}/login`,
      {email:email, password:password})
      .do(user=> this.user = user)
  }
  isLoggedIn():boolean{
    return this.user !== undefined
  }
  handleLogin(path:string = this.lastUrl){
    this.router.navigate(['/login',btoa(path)])//btoa so esconde o path na url
  }

}
