import { HttpInterceptor,HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Injectable, Injector } from "@angular/core";
import { LoginService } from "app/security/login/login.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(private injector: Injector){
  }

//o obj representa o proximo interceptor na fila de interceptos ou o ultimo obj q é o oj responsavel a chamada final
// headers = headers.set('Authorization',`Bearer ${this.loginService.user.acessToken}`)
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //estamos usando o injector pq deu erro de dependenncia ciclica quando injetmaos pelo contrutor
    const loginService = this.injector.get(LoginService)
    if(loginService.isLoggedIn()){
      //temos que chamar o clone pq o obj request é imutavel
      const authRequest = req.clone(
        {setHeaders:{'Authorization':`Bearer ${loginService.user.acessToken}`}})
      return next.handle(authRequest)
    }else{
      return next.handle(req)
    }
    // console.log('interceptando', req)
  }

}
